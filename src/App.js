import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div clasname="body">
        <header>
          <h1 contenteditable>Header.com</h1>
        </header>
        <div class="left-sidebar" contenteditable>Left Sidebar
        </div>
        <main contenteditable>
        
        </main>
        <div class="right-sidebar" contenteditable>Right Sidebar
        </div>
        <footer contenteditable>Footer Content — Header.com 2020
        </footer>
    </div>

  );
}

export default App;
